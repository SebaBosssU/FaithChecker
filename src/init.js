require([
    'two/ready',
    'two/faithChecker',
    'Lockr',
    'two/eventQueue',
    'two/faithChecker/ui',
], function (
    ready,
    faithChecker,
    Lockr,
    eventQueue
) {
    if (faithChecker.isInitialized()) {
        return false
    }

    ready(function () {
        faithChecker.init()
        faithChecker.interface()
        
        ready(function () {
            if (Lockr.get('priest-active', false, true)) {
                faithChecker.start()
            }

            eventQueue.bind('Priest/started', function () {
                Lockr.set('priest-active', true)
            })

            eventQueue.bind('Priest/stopped', function () {
                Lockr.set('priest-active', false)
            })
        }, ['initial_village'])
    })
})
