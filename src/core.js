define('two/faithChecker', [
    'two/eventQueue',
    'two/locale',
    'two/utils',
    'Lockr',
    'conf/buildingTypes',
    'conf/locationTypes'
], function(
    eventQueue,
    Locale,
    utils,
    Lockr,
    BUILDING_TYPES,
    LOCATION_TYPES
) {
    var modelDataService = injector.get('modelDataService')
    var socketService = injector.get('socketService')
    var routeProvider = injector.get('routeProvider')
    var initialized = false
    var running = false
    var recall = true
    var chapelBlockade = 0
	
    getHighestGodsHouseLevel = function getHighestGodsHouseLevel(villages) {
        var villageIdx,
            highestLevel = 0,
            tmpLevel

        for (villageIdx = 0; villageIdx < villages.length; villageIdx++) {
            tmpLevel = villages[villageIdx].chapel || villages[villageIdx].church
            if (tmpLevel && (tmpLevel > highestLevel)) {
                highestLevel = tmpLevel
            }
        }
        return highestLevel
    }
	
    getMoralBonus = function getMoralBonus(level, opt_isChapel) {
        var bonusLookUp = modelDataService.getWorldConfig().getChurchBonus(),
            bonusFactor = 0
        if ((level < 0) || (!opt_isChapel && level > (bonusLookUp.length - 1))) {
            return 0
        }
        if (opt_isChapel) {
            bonusFactor = modelDataService.getWorldConfig().getChapelBonus()
        } else {
            bonusFactor = bonusLookUp[level]
        }
        return Math.floor(bonusFactor * 100)
    }
	
    requestVillageProvinceNeighbours = function requestVillageProvinceNeighbours(villageId, callback) {
        socketService.emit(routeProvider.VILLAGES_IN_PROVINCE, {
            'village_id': villageId
        }, callback)
    }

    function faithInfo() {
        var player = modelDataService.getSelectedCharacter()
        var villages = player.getVillageList()

        villages.forEach(function(village) {
            var villageid = village.data.villageId
            var provinceid = village.data.province_id
            var isChapel = village.data.buildings.chapel.level
            var buildingQueue = village.buildingQueue.data.queue
            var resources = village.getResources()
            var computed = resources.getComputed()
            var maxStorage = resources.getMaxStorage()
            var food = computed.food
            var wood = computed.wood
            var clay = computed.clay
            var iron = computed.iron
            var villageFood = food.currentStock
            var villageWood = wood.currentStock
            var villageClay = clay.currentStock
            var villageIron = iron.currentStock
            var foodCost = [0, 5000]
            var woodCost = [160, 16000]
            var clayCost = [200, 20000]
            var ironCost = [50, 5000]
            if (isChapel == 1) {
                chapelBlockade = 1
            }

            requestVillageProvinceNeighbours(villageid, function(responseData) {
                villagesClosest = responseData.villages
                highestChapelLevel = getHighestGodsHouseLevel(responseData.villages)
                bonus = getMoralBonus(highestChapelLevel, isChapel === 1)
                console.log(bonus)
                if (bonus == 50 && (buildingQueue === undefined || buildingQueue.length == 0)) {
                    if (chapelBlockade = 1) {
                        if (villageWood >= woodCost[1] && villageClay >= clayCost[1] && villageIron >= ironCost[1] && villageFood >= foodCost[1]) {
                            socketService.emit(routeProvider.VILLAGE_UPGRADE_BUILDING, {
                                building: 'church',
                                village_id: villageid,
                                location: LOCATION_TYPES.MASS_SCREEN,
                                premium: false
                            })
                            utils.emitNotif('success', Locale('priest', 'church'))
                        } else {
                            utils.emitNotif('error', Locale('priest', 'resources'))
                        }
                    } else {
                        if (villageWood >= woodCost[0] && villageClay >= clayCost[0] && villageIron >= ironCost[0] && villageFood >= foodCost[0]) {
                            socketService.emit(routeProvider.VILLAGE_UPGRADE_BUILDING, {
                                building: 'chapel',
                                village_id: villageid,
                                location: LOCATION_TYPES.MASS_SCREEN,
                                premium: false
                            })
                            utils.emitNotif('success', Locale('priest', 'chapel'))
                        } else {
                            utils.emitNotif('error', Locale('priest', 'resources'))
                        }
                    }
                } else if (bonus == 50 && buildingQueue) {
                    buildingQueue.forEach(function(queue) {
                        var faithMax = queue.building

                        if ((faithMax != 'chapel' || faithMax != 'church') && chapelBlockade == 0) {
                            if (villageWood >= woodCost[0] && villageClay >= clayCost[0] && villageIron >= ironCost[0] && villageFood >= foodCost[0]) {
                                socketService.emit(routeProvider.VILLAGE_UPGRADE_BUILDING, {
                                    building: 'chapel',
                                    village_id: villageid,
                                    location: LOCATION_TYPES.MASS_SCREEN,
                                    premium: false
                                })
                                utils.emitNotif('success', Locale('priest', 'chapel'))
                            } else {
                                utils.emitNotif('error', Locale('priest', 'resources'))
                            }
                        } else if ((faithMax != 'chapel' || faithMax != 'church') && chapelBlockade == 1) {
                            if (villageWood >= woodCost[1] && villageClay >= clayCost[1] && villageIron >= ironCost[1] && villageFood >= foodCost[1]) {
                                socketService.emit(routeProvider.VILLAGE_UPGRADE_BUILDING, {
                                    building: 'church',
                                    village_id: villageid,
                                    location: LOCATION_TYPES.MASS_SCREEN,
                                    premium: false
                                })
                                utils.emitNotif('success', Locale('priest', 'church'))
                            } else {
                                utils.emitNotif('error', Locale('priest', 'resources'))
                            }
                        }
                    })
                } else {
                    utils.emitNotif('success', Locale('priest', 'full'))
                }
            })
        })
    }

    var faithChecker = {}
    faithChecker.init = function() {
        initialized = true
    }
    faithChecker.start = function() {
        eventQueue.trigger('Priest/started')
        running = true
        faithInfo()
    }
    faithChecker.stop = function() {
        eventQueue.trigger('Priest/stopped')
        running = false
    }
    faithChecker.isRunning = function() {
        return running
    }
    faithChecker.isInitialized = function() {
        return initialized
    }
    return faithChecker
})