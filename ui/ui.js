define('two/faithChecker/ui', [
    'two/faithChecker',
    'two/FrontButton',
    'two/locale',
    'two/utils',
    'two/eventQueue'
], function (
    faithChecker,
    FrontButton,
    Locale,
    utils,
    eventQueue
) {
    var opener

    function PriestInterface () {
        Locale.create('priest', __priest_locale, 'pl')
        
        opener = new FrontButton(Locale('priest', 'title'), {
            classHover: false,
            classBlur: false,
            tooltip: Locale('priest', 'description')
        })

        opener.click(function () {
            if (faithChecker.isRunning()) {
                faithChecker.stop()
                utils.emitNotif('success', Locale('priest', 'deactivated'))
            } else {
                faithChecker.start()
                utils.emitNotif('success', Locale('priest', 'activated'))
            }
        })

        eventQueue.bind('Priest/started', function () {
            opener.$elem.removeClass('btn-green').addClass('btn-red')
        })

        eventQueue.bind('Priest/stopped', function () {
            opener.$elem.removeClass('btn-red').addClass('btn-green')
        })

        if (faithChecker.isRunning()) {
            eventQueue.trigger('Priest/started')
        }

        return opener
    }

    faithChecker.interface = function () {
        faithChecker.interface = PriestInterface()
    }
})
